export SRCDIR=$PWD
export PYTHONPATH=$SRCDIR/instdir/program:unotest/source/python
export URE_BOOTSTRAP=file://$SRCDIR/instdir/program/fundamentalrc
export TestUserDir=file:///tmp
# export TDOC=$SRCDIR/sw/qa/uitest/data
export TEST_DIR=$SRCDIR/uitest/test_uno.py
export SAL_USE_VCLPLUGIN=gen
export LC_ALL=C

rm -rf /tmp/libreoffice/4

python3 "$SRCDIR"/uitest/test_main.py --soffice=path:"$SRCDIR"/instdir/program/soffice --userdir=file:///tmp/libreoffice/4 --file="$TEST_DIR"

# $ bash ./execute.sh UITEST_TEST_NAME="FILENAME.CLASS_NAME.TEST_NAME"
